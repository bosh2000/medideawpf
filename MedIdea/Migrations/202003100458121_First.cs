namespace MedIdea.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GenderPatients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Gender = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Surname = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        Address = c.String(),
                        Phone = c.String(),
                        Gender_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GenderPatients", t => t.Gender_Id)
                .Index(t => t.Gender_Id);
            
            CreateTable(
                "dbo.Visits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateVisit = c.DateTime(nullable: false),
                        Patient_Id = c.Int(),
                        TypeVisit_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .ForeignKey("dbo.TypeVisits", t => t.TypeVisit_Id)
                .Index(t => t.Patient_Id)
                .Index(t => t.TypeVisit_Id);
            
            CreateTable(
                "dbo.TypeVisits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Visits", "TypeVisit_Id", "dbo.TypeVisits");
            DropForeignKey("dbo.Visits", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.Patients", "Gender_Id", "dbo.GenderPatients");
            DropIndex("dbo.Visits", new[] { "TypeVisit_Id" });
            DropIndex("dbo.Visits", new[] { "Patient_Id" });
            DropIndex("dbo.Patients", new[] { "Gender_Id" });
            DropTable("dbo.TypeVisits");
            DropTable("dbo.Visits");
            DropTable("dbo.Patients");
            DropTable("dbo.GenderPatients");
        }
    }
}
