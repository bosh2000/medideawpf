namespace MedIdea.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMiddleNameAndNamePatient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "Name", c => c.String());
            AddColumn("dbo.Patients", "MiddleName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Patients", "MiddleName");
            DropColumn("dbo.Patients", "Name");
        }
    }
}
