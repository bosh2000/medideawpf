﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedIdea.Model
{
    public class GenderPatient
    {
        public int Id { get; set; }
        public string Gender { get; set; }
    }
}
