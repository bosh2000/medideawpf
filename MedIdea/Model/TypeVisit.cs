﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedIdea.Model
{
    public class TypeVisit
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
