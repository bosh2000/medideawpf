﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedIdea.Model
{
    public class Visit
    {
        public int Id { get; set; }
        public virtual Patient Patient { get; set; }
        public DateTime DateVisit { get; set; }
        public virtual TypeVisit TypeVisit { get; set; }

        public string Diagnosis { get; set; }
    }
}
