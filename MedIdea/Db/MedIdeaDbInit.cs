﻿using MedIdea.Model;
using System;
using System.Data.Entity;

namespace MedIdea.Db
{
    public class MedIdeaDbInit : DropCreateDatabaseAlways<MedIdeaContext>
    {
        protected override void Seed(MedIdeaContext context)
        {
            context.Genders.Add(new GenderPatient
            {
                Gender = "Мужской"
            });

            context.Genders.Add(new GenderPatient
            {
                Gender = "Женский"
            });

            context.SaveChanges();

            context.TypeVisits.Add(new TypeVisit { Type = "Первичный" });
            context.TypeVisits.Add(new TypeVisit { Type = "Вторичный" });
            context.SaveChanges();

            context.Visits.Add(new Visit
            {
                DateVisit = DateTime.Parse("12.01.2020"),
                Patient = context.Patients.Find(3),
                TypeVisit=context.TypeVisits.Find(1),
               
            });

            context.Patients.Add(new Patient
            {
                Id = 1,
                Name = "Юрий",
                Surname = "Пономаренко",
                MiddleName = "Платонович",
                Address = "г.Кардымово, ул. Достоевского, дом 32, квартира 6",
                BirthDate = DateTime.Parse("12.04.1964"),
                Gender = context.Genders.Find(1),
                Phone = "8 (456) 259-51-85"
            });
            context.Patients.Add(new Patient
            {
                Id = 2,
                Name = "Йошка",
                Surname = "Рогов",
                MiddleName = "Викторович",
                Address = "г.Сковородино, ул. Советская, дом 64, квартира 295",
                BirthDate = DateTime.Parse("06.04.1968"),
                Gender = context.Genders.Find(1),
                Phone = "8 (542) 549-98-24"
            });

            context.Patients.Add(new Patient
            {
                Id = 4,
                Name = "Роберт",
                Surname = "Пахомов",
                MiddleName = "Владимирович",
                Address = "г.Приморско-Ахтарск, ул. Хрущёва, дом 47, квартира 30",
                BirthDate = DateTime.Parse("06.04.1968"),
                Gender = context.Genders.Find(1),
                Phone = "8 (175) 945-93-49"
            });

            context.Patients.Add(new Patient
            {
                Id = 5,
                Name = "Йонас",
                Surname = "Миклашевский",
                MiddleName = "Леонидович",
                Address = "г.Чарышское, ул. Зелёная, дом 95, квартира 1",
                BirthDate = DateTime.Parse("13.10.1967"),
                Gender = context.Genders.Find(1),
                Phone = "8 (548) 837-15-67"
            });

            context.Patients.Add(new Patient
            {
                Id = 6,
                Name = "Зуфар",
                Surname = "Львович",
                MiddleName = "Леонидович",
                Address = "г.Чегдомын, ул. Журавлёва, дом 46, квартира 261",
                BirthDate = DateTime.Parse("27.06.1976"),
                Gender = context.Genders.Find(1),
                Phone = "8 (149) 699-18-78"
            });
            context.Patients.Add(new Patient
            {
                Id = 7,
                Name = "Платон",
                Surname = "Сергеев",
                MiddleName = "Эдуардович",
                Address = "г.Апатиты, ул. Молодёжная, дом 27, квартира 268",
                BirthDate = DateTime.Parse("27.05.1976"),
                Gender = context.Genders.Find(1),
                Phone = "8 (712) 129-56-21"
            });

            context.Patients.Add(new Patient
            {
                Id = 8,
                Name = "Юлий",
                Surname = "Белозёров",
                MiddleName = "Данилович",
                Address = "г.Левокумское, ул. Заречная, дом 46, квартира 298",
                BirthDate = DateTime.Parse("27.05.1986"),
                Gender = context.Genders.Find(1),
                Phone = "8 (472) 680-71-29"
            });

            context.Patients.Add(new Patient
            {
                Id = 9,
                Name = "Цезарь",
                Surname = "Коновалов",
                MiddleName = "Станиславович",
                Address = "г.Ивановка, ул. Заречная, дом 32, квартира 137",
                BirthDate = DateTime.Parse("10.11.1995"),
                Gender = context.Genders.Find(1),
                Phone = "8 (515) 270-53-72"
            });

            context.Patients.Add(new Patient
            {
                Id = 10,
                Name = "Эрик",
                Surname = "Павленко",
                MiddleName = "Михайлович",
                Address = "г.Орел, ул. Советская, дом 62, квартира 56",
                BirthDate = DateTime.Parse("10.07.1987"),
                Gender = context.Genders.Find(1),
                Phone = "8 (862) 332-83-90"
            });

            context.Patients.Add(new Patient
            {
                Id = 11,
                Name = "Чеслав",
                Surname = "Власов",
                MiddleName = "Ярославович",
                Address = "г.Аян, ул. Зелёная, дом 73, квартира 119",
                BirthDate = DateTime.Parse("10.01.1974"),
                Gender = context.Genders.Find(1),
                Phone = "8 (542) 390-29-30"
            });

            context.SaveChanges();
        }
    }
}