﻿using MedIdea.Db;
using MedIdea.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedIdea
{
    public class MedIdeaContext :DbContext
    {
        public MedIdeaContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new MedIdeaDbInit());
            Database.Initialize(true);
            Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Visit> Visits { get; set; }
        
        public DbSet<GenderPatient> Genders { get; set; }
        public DbSet<TypeVisit> TypeVisits { get; set; }
    }
}
